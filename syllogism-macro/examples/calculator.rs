// This example illustrates how the `syllogism` and the `syllogism-macro` crate can be used
// as a workaround for specialization.
// In the example, we act as if the modules `crate_*` are in fact separate crates.
// The example should still work if they really are (i.e.: no violation of the orphan rules, ...).
extern crate syllogism;
extern crate syllogism_macro;

#[macro_use]
mod crate_calculator_core {
    use std::io::BufRead;
    pub trait EventHandler<E> {
        // Note that the event is by value. This allows specialization based on the `Specialize`
        // trait, as we will see below.
        fn handle_event(&mut self, event: E);
    }

    pub struct InputEvent {
        pub data: String,
    }

    pub struct EndOfInputEvent;

    pub struct InputErrorEvent {
        pub error: std::io::Error,
    }

    pub struct Calculator<C>
    where
        C: EventHandler<InputEvent> + EventHandler<InputErrorEvent> + EventHandler<EndOfInputEvent>,
    {
        child: C,
    }

    impl<C> Calculator<C>
    where
        C: EventHandler<InputEvent> + EventHandler<InputErrorEvent> + EventHandler<EndOfInputEvent>,
    {
        pub fn new(child: C) -> Self {
            Self { child }
        }

        pub fn run(&mut self) {
            let stdin = std::io::stdin();
            for line in stdin.lock().lines() {
                match line {
                    Ok(l) => {
                        let event = InputEvent { data: l };
                        self.child.handle_event(event);
                    }
                    Err(e) => {
                        let event = InputErrorEvent { error: e };
                        self.child.handle_event(event);
                    }
                }
            }
            self.child.handle_event(EndOfInputEvent);
        }
    }

    // Each "crate" that gets added to the "ecosystem" gets an extra line here.
    syllogism::define_compatibility!(
        $
        (NotInCore, macro_for_core),
        (NotInSimpleCalculator, macro_for_simple_calculator),
        (NotInEventConfirmation, macro_for_event_confirmation),
        (NotInResultPrinter, macro_for_result_printer),
    );

    crate::syllogism_macro::impl_specialization!(
        trait NotInCore;
        macro macro_for_core;
        type InputEvent;
        type EndOfInputEvent;
        type InputErrorEvent;
    );
}

mod crate_simple_calculator {
    use syllogism::IsNot;
    // This imports all the types that are defined in the `macro_for_simple_calculator`.
    // Because it's a list that can grow, we must use a wildcard import here.
    use crate::crate_calculator_core::*;

    crate::syllogism_macro::impl_specialization!(
        trait NotInSimpleCalculator;
        macro macro_for_simple_calculator;
        type NumberEvent;
    );

    pub struct NumberEvent {
        pub number: i64,
    }

    pub struct NumberParser<C> {
        child: C,
    }

    impl<C> NumberParser<C> {
        pub fn new(child: C) -> Self {
            Self { child }
        }
    }

    // The generic implementation.
    impl<C, E> EventHandler<E> for NumberParser<C>
    where
        C: EventHandler<E>,
        E: IsNot<InputEvent>,
    {
        fn handle_event(&mut self, event: E) {
            self.child.handle_event(event);
        }
    }

    // The specific implementation.
    impl<C> EventHandler<InputEvent> for NumberParser<C>
    where
        C: EventHandler<InputEvent> + EventHandler<NumberEvent>,
    {
        fn handle_event(&mut self, event: InputEvent) {
            use std::str::FromStr;
            if let Ok(number) = i64::from_str(&event.data) {
                let number_event = NumberEvent { number };
                self.child.handle_event(number_event);
            } else {
                self.child.handle_event(event);
            }
        }
    }
}

mod crate_event_confirmation {
    use crate::crate_calculator_core::{EventHandler, InputEvent};
    use syllogism::{Distinction, IsNot, Specialize};

    // This crate does not define a special event type, so no need to define that it's
    // compatible with the others.

    pub struct EventConfirmationDemander<E, C> {
        event_to_be_confirmed: Option<E>,
        child: C,
    }

    impl<E, C> EventConfirmationDemander<E, C>
    where
        E: IsNot<InputEvent>, // It doesn't make sense to block an InputEvent.
    {
        pub fn new(child: C) -> Self {
            Self {
                child,
                event_to_be_confirmed: None,
            }
        }
    }

    impl<E, C> EventHandler<InputEvent> for EventConfirmationDemander<E, C>
    where
        C: EventHandler<InputEvent> + EventHandler<E>,
    {
        fn handle_event(&mut self, event: InputEvent) {
            if self.event_to_be_confirmed.is_none() {
                self.child.handle_event(event);
            } else {
                match event.data.as_ref() {
                    "y" => {
                        self.child.handle_event(
                            self.event_to_be_confirmed
                                .take()
                                .expect("We're not in the 'is_none()' branch here."),
                        );
                    }
                    "n" => {
                        self.event_to_be_confirmed = None;
                        println!("Cancelled.");
                    }
                    _ => {
                        println!("Sorry, I don't understand.");
                    }
                }
            }
        }
    }

    impl<GE, E, C> EventHandler<GE> for EventConfirmationDemander<E, C>
    where
        GE: IsNot<InputEvent> + Specialize<E>,
        C: EventHandler<GE> + EventHandler<E>,
    {
        fn handle_event(&mut self, event: GE) {
            if self.event_to_be_confirmed.is_some() {
                println!("Please first answer my question.");
                return;
            }

            match event.specialize() {
                Distinction::Generic(g) => {
                    self.child.handle_event(g);
                }
                Distinction::Special(s) => {
                    self.event_to_be_confirmed = Some(s);
                    println!("Are you sure you want to do this (y/n)?");
                }
            }
        }
    }
}

mod crate_result_printer {
    // This crate does not define a special event type, so there's no need to define that it's
    // compatible with the other types.

    use crate::crate_calculator_core::{
        EndOfInputEvent, EventHandler, InputErrorEvent, InputEvent,
    };
    use crate::crate_simple_calculator::NumberEvent;
    use syllogism::IsNot;

    pub struct ResultPrinter {}

    impl ResultPrinter {
        pub fn new() -> Self {
            ResultPrinter {}
        }
    }

    impl EventHandler<InputEvent> for ResultPrinter {
        fn handle_event(&mut self, event: InputEvent) {
            println!("Unknown command: {}", event.data);
        }
    }

    impl EventHandler<InputErrorEvent> for ResultPrinter {
        fn handle_event(&mut self, _event: InputErrorEvent) {
            println!("An error occurred.");
        }
    }

    impl EventHandler<EndOfInputEvent> for ResultPrinter {
        fn handle_event(&mut self, _event: EndOfInputEvent) {
            println!("Bye!");
        }
    }

    impl EventHandler<NumberEvent> for ResultPrinter {
        fn handle_event(&mut self, event: NumberEvent) {
            println!("The result is: {}", event.number);
        }
    }

    impl<E> EventHandler<E> for ResultPrinter
    where
        E: IsNot<InputEvent> + IsNot<InputErrorEvent> + IsNot<EndOfInputEvent> + IsNot<NumberEvent>,
    {
        fn handle_event(&mut self, _event: E) {
            println!("Unknown event. This is probably an implementation problem.");
        }
    }
}

fn main() {
    use crate_calculator_core::*;
    use crate_event_confirmation::EventConfirmationDemander;
    use crate_result_printer::ResultPrinter;
    use crate_simple_calculator::{NumberEvent, NumberParser};

    let mut calculator = Calculator::new(NumberParser::new(EventConfirmationDemander::<
        NumberEvent,
        _,
    >::new(ResultPrinter::new())));
    calculator.run();
}
