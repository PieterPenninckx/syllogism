# Syllogism-macro

[`Syllogism`](https://crates.io/crates/syllogism) is a crate that allows for some specialization 
using stable Rust.
Syllogism requires a huge amount of boilerplate.
The syllogism-macro has some macros that can generate this boilerplate for you.

See the documentation for more details.

## Status of the project

`syllogism` and `syllogism-macro` are experimental crates and not really actively maintained.
See the README of the `syllogism` crate for more information.


Contributing
------------

We welcome contributions, both in the form of issues and in the form of merge requests.
Before opening a merge request, please open an issue first so that you know whether a subsequent
merge request would likely be approved.


License
-------

This crate is distributed under the terms of the MIT license or the Apache License (Version 2.0),
at your choice.
For the application of the MIT license, the examples included in the doc comments are not
considered "substantial portions of this Software".
