use syllogism_macro::impl_specialization;
extern crate syllogism;
use syllogism::{IsNot, Specialize};

fn isnot_check<T, U: IsNot<T>>() {}
fn specialize_check<T, U: Specialize<T>>() {}

#[allow(dead_code)]
struct S1 {}
#[allow(dead_code)]
struct S2 {}

impl_specialization!(type S1; type S2);

#[test]
fn isnot_is_implemented() {
    isnot_check::<S1, S2>();
    isnot_check::<S2, S1>();
}

#[test]
fn specialize_is_implemented() {
    specialize_check::<S1, S1>();
    specialize_check::<S1, S2>();
    specialize_check::<S2, S1>();
    specialize_check::<S2, S2>();
}

#[test]
fn macro_does_not_implement_isnot_self() {
    // Test that IsNot<Self> is not implemented.
    trait TestTrait<T> {}
    // The following impl blocks overlap when `S1` implements `IsNot<S1>`.
    impl<T: IsNot<S1>> TestTrait<T> for S1 {}
    impl TestTrait<S1> for S1 {}
}

mod m1 {
    pub struct S1 {}
}

mod m2 {
    pub struct S2 {}
}

impl_specialization!(type m1::S1; type m2::S2);

#[test]
fn isnot_is_implemented_for_structs_in_modules() {
    isnot_check::<m1::S1, m2::S2>();
    isnot_check::<m2::S2, m1::S1>();
}

pub struct S3<T> {
    #[allow(dead_code)]
    t: T,
}
pub struct S4<T> {
    #[allow(dead_code)]
    t: T,
}

struct S3bis {}
struct S4bis {}

impl_specialization!(type S3<T>; type S3bis; type S4<U>; type S4bis);

#[test]
fn impl_isnot_works_for_generic_structs() {
    isnot_check::<S3<u8>, S3bis>();
    isnot_check::<S3<u8>, S4<u8>>();
    isnot_check::<S3<u8>, S4bis>();

    isnot_check::<S3bis, S3<u8>>();
    isnot_check::<S3bis, S4<u16>>();
    isnot_check::<S3bis, S4bis>();

    isnot_check::<S4<u16>, S3<u16>>();
    isnot_check::<S4<u16>, S3bis>();
    isnot_check::<S4<u16>, S4bis>();

    isnot_check::<S4bis, S3<u8>>();
    isnot_check::<S4bis, S4<u16>>();
    isnot_check::<S4bis, S3bis>();
}

#[test]
fn impl_specialize_works_for_generic_structs() {
    specialize_check::<S3<u8>, S3bis>();
    specialize_check::<S3<u8>, S4<u8>>();
    specialize_check::<S3<u8>, S4bis>();

    specialize_check::<S3bis, S3bis>();
    specialize_check::<S3bis, S3<u8>>();
    specialize_check::<S3bis, S4<u16>>();
    specialize_check::<S3bis, S4bis>();

    specialize_check::<S4<u16>, S3<u16>>();
    specialize_check::<S4<u16>, S3bis>();
    specialize_check::<S4<u16>, S4bis>();

    specialize_check::<S4bis, S4bis>();
    specialize_check::<S4bis, S3<u8>>();
    specialize_check::<S4bis, S4<u16>>();
    specialize_check::<S4bis, S3bis>();
}

pub struct S5<'a> {
    #[allow(dead_code)]
    u: &'a u8,
}

pub struct S6<'b> {
    #[allow(dead_code)]
    u: &'b u8,
}

impl_specialization!(type S5<'a>; type S6<'b>);

#[test]
fn impl_isnot_works_for_structs_with_lifetimes() {
    isnot_check::<S5, S6>();
    isnot_check::<S6, S5>();
}
